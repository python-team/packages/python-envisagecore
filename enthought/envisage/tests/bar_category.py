""" A test class used to test categories. """


# Enthought library imports.
from enthought.traits.api import HasTraits, Int


class BarCategory(HasTraits):
    y = Int

#### EOF ######################################################################
