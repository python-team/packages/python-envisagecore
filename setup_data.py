INFO = {
    'name': 'EnvisageCore',
    'version': '3.2.0',
    'install_requires': [
        'AppTools >= 3.4.1.dev',
        'Traits >= 3.6.0.dev',
    ],
}
