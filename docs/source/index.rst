Envisage Core Documentation
==============================================

.. toctree::
    :maxdepth: 2
	
    introduction.rst
    core.rst
    extension_points.rst
    services.rst
    plugins.rst
    howto_create_a_plugin.rst
    message_of_the_day.rst
    message_of_the_day_(using_eggs).rst
    workbench.rst
    glossary.rst

* :ref:`search`
